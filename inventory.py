
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pygame
from pygame.locals import *
from list import *
from inventorybomb import *

class Inventory(object):
    def __init__(self):
        self.background = pygame.image.load('./pics/room1.1.bmp').convert()
        self.list = List()
        self.list.addobject(Inventorybomb())

        self.rectimage = pygame.image.load('./pics/rect1.bmp').convert()
        self.rectimage.set_colorkey((0,0,0))
        self.selectioncounter = 1

    def draw(self,screen):

        screen.scr.blit(self.background, (0, 0))

        self.drawrect(screen,self.selectioncounter*80,120)

        for i in range(0,self.list.length):
            o = self.list.getwithindex(i)
# pass by value FIXME
#             o = None
#             o = self.list.getlistobject(o)
            if o:
                o.draw(screen,80*(i+1),120)



    def drawrect(self,screen,x,y):
        screen.scr.blit(self.rectimage, (x, y))


    def moveleft(self):
        if self.selectioncounter > 1:
            self.selectioncounter -= 1

    def moveright(self):
        if self.selectioncounter < 8:
            self.selectioncounter += 1

#    def setselection(self):
#        i = 
        
    def getitem(self,o):
#        for i in range(0,self.selectioncounter):
#            o = None
#            o = self.list.getlistobject(o)
        o = self.list.getwithindex(self.selectioncounter-1) 
        if o:
            return o

        return None

    def additem(self,o):
        self.list.addobject(o)
