
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from list import *
from box import *
from block import *
from exitbox import *

from utility import *

from entityfactory import *

import inspect

class Room:
    ""
    def __init__(self):
        self.initroom()
        self.entityfactory = None

    # FIXME
    def addfactory(self,factory):
        frame = inspect.currentframe()
        try:
            frame != None
        finally:
            factory.add(self)

    def loadroom(self,roommanager):
         roommanager.loadroom(self)

    def updatelist(self,list):
        for el in list.list:
            if el:
                el.update(self)

    def addbox(self,x,y,w,h):
        self.boxes.add(Box(x,y,w,h))

    def addblock(self,x,y,z,w,h,d):
        self.blocks.add(Block(x,y,z,w,h,d))

    def addexit(self,x,y,w,h,roomid,newx,newy,room):
         self.exits.add(Exitbox(x,y,w,h,roomid,newx,newy,room))

    def addentitythroughfactory(self,room):
        1#FIXME

    def addentity(self,entity):
        if entity:
            self.entities.add(entity)

    def addenemy(self,enemy):
        self.addentity(enemy)

    def addsprite(self,sprite):
        self.addentity(sprite)

    def loadwalls(self):
        1

    def initroom(self):
	print "initroom"
        self.totalboxes = 0
        self.boxes = List()
        self.blocks = List()
        self.totalexits = 0
        self.exits = List()
        self.background = None
        self.entities = List()
        self.idcounter = 0
        self.night = 0
        self.loadwalls()
	

    def draw(self,screen):
        screen.scr.blit(self.background, (0, 0))
        screen.draw(self.entities)

#    def auxdraw(self):
#	self.auxdraw(self.)
#        screen.scr.blit(self.background, (0, 0))
#        screen.draw(self.entities)

    def update(self,player,level,swordplay):#unsused level
        self.updatelist(self.entities)

        o = player.collisionlist(self.entities)
	if o:
	    #o.dofood(player)
	    o.remove(self.entities)
 
    def collisionplayer(self,mouse,player):
        mouse.collisionplayer(player)

    def domouse(self,game):
        # Collide mouse with player

        p = game.domouseonplayer(self)
	if p:
            print("mouse click on player 1")
            foo = game.leftbuttonpressed(game.player)
            if foo:
                print("mouse click on player 2")
                return
            
        # Collide mouse with entities
        o = None
        o = game.collisionlistonmouse(self.entities)
	#print self.entities.getwithindex(0)
        if o:
            foo = game.leftbuttonpressed(o)
#            print foo
            if foo:
                game.screen.scr.blit(game.font.render(foo, 16, (255,255,255)), (10,10))
#                print foo
            else:
                game.screen.scr.blit(game.font.render(o.getname(game.text), 10, (255,255,255)), (game.mouse.x+10,game.mouse.y))
#            mouse.middlebuttonpressed(o,text)
#            mouse.rightbuttonpressed(o,text)
            


    def domouseinventory(self,game):#,screen,mouse,font,text,item,player):
        # Collide mouse with player
        p = game.domouseonplayer(self)#game.mouse.collisionplayer(game)
        if p:
            foo = game.mouse.leftbuttonpressedwithinventoryitem(game.player,game)
            if foo:
                return foo 

        # Collide mouse with entities
        o = None
        o = game.collisionlistonmouse(self.entities)
        if o and game.inventoryitem:
            foo = game.mouse.leftbuttonpressedwithinventoryitem(o,game)#.text,game.inventoryitem)
            if foo:
                return foo
        if not o and game.mouse.press(0):
            return None#1

        return None
#             if foo:
#                 screen.scr.blit(font.render(foo, 16, (255,255,255)), (10,10))
# #                print foo
#             else:
#                 screen.scr.blit(font.render(o.getname(text), 10, (0,0,0)), (mouse.x+10,mouse.y))
#            mouse.middlebuttonpressed(o,text)
#            mouse.rightbuttonpressed(o,text)



    def moveup(self, player):
        player.moveupanddirect1()

    def movedown(self, player):
        player.movedownanddirect1()

    def moveleft(self, player):
        player.moveleftanddirect1()

    def moveright(self, player):
        player.moverightanddirect1()


######## COLLISION CODE

    def collisionlistbomb(self,bomb,list):
#         for i in range(0,list.length):
#             o = None
#             o = list.getlistobject(o)

        for o in list.list:
            if o and o.x +12>= bomb.x - bomb.w and o.x <= bomb.x + bomb.w +12and o.y+12 >= bomb.y - bomb.h and o.y <= bomb.y + bomb.h +12:
                return o
        return None


    def collisionexits(self,player,roommanager):
        o = player.collisionlist(self.exits)

        if o:

            print 'foo x=%d,y=%d px=%d py=%d' % (o.x,o.y,player.x,player.y)

            o.room.loadroom()
            roommanager.room = o.room
            player.warp(o.newx,o.newy)
            return 1
        return None

    def collisionboxes(self,player):
        o = player.collisionlist(self.boxes)
        
        if o:
            #player.bounceback()#FIXME1 level arg
            return 1
        return None

    def collisionblocks(self,player):
        o = player.collisionlistblocks(self.blocks)
        
        if o:
            #player.bounceback()#FIXME1 level arg
            return 1
        return None


    def collisionandremoveenemieswithbomb(self, bomb):
        o = self.collisionlistbomb(bomb,self.entities)
        if o:
            o.hit()
            o.remove(self.entities)
            self.entities.remove(o)
            
    def summonskeletonunderplayercommand(self,x,y):
        self.addenemy(Skeletonunderplayercommand(x,y,50,50))


    def summonskeleton(self,x,y):
        self.addenemy(Skeleton2(x,y,50,50))

    def removeallenemies(self):
        self.entities = List()
            
    def removeallentities(self):
        self.entities = List()


