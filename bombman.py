
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pygame
from math import *
from engine import *
from enemytalking import *
from talksystembombman import *
from list import *

class Bombman(Enemytalking):
    "Bombman"
    def __init__(self,x,y,w,h, boxes, entities, player):
        Enemytalking.__init__(self,x,y,w,h,Talksystembombman())
        ###self.images = List()

	self.image = pygame.image.load("./pics/bombman1-333x333.bmp").convert()
        self.image.set_colorkey((0,0,0))
	
	self.addimage("./pics/bombman1-200x200.bmp",0,0,0)

    def draw3(self,color):
	i = 0
	j = 0
	n = 10
	cx = 100
	cy = 100
	PI = 3.14152829
	r = 100
	theta1 = PI/2 
	y = 0	
	while i < n:
		theta1 -= .01 
		xx = i
		xx *= sin(theta1)
		x = xx
		 
		pygame.draw.line(pygame.display.get_surface(),color, (cx+x*r,cy+y*r),(cx-x*r,cy+y*r))
		y += 1	
		i += .001

    def draw2(self,color):
	rotx = 0#sqrt(2)/2 
	roty = 0#sqrt(2)/2 
	rotz = 0#sqrt(2)/2 

	self.i = 0 ## / medridians, / paralells
	self.j = 0###-2*3.14 ## / medridians, / paralells
	self.PI = 3.14152829
	self.cx = 100
	self.cy = 100
	self.cz = 100
	self.n = 10
	self.r = 100
	# NOTE or enlarge radius r before 2 PI loops
	while rotx < 2*self.PI:
		rotx += .01
		roty += .01
		rotz += .01
		e = engine(rotx,roty,rotz)
		while self.j < self.n / 2:
			self.theta1 = self.j*2*self.PI / self.n - self.PI
			self.theta2 = (self.j+1)*2*self.PI / self.n - self.PI
			while self.i < self.n:
				self.theta3 = self.i * 2*self.PI / self.n
				self.x = cos(self.theta2)*cos(self.theta3) 
				self.y = sin(self.theta2)*sin(self.theta3)
				self.z = sin(self.theta2)
				r1 = Vector3(self.x,self.y,self.z)
				if rotx:
					r1 = e.mx.multiply(r1)
				if roty:
					r1 = e.my.multiply(r1)
				if rotz:
					r1 = e.mz.multiply(r1)
				self.x = r1.array[0]	
				self.y = r1.array[1]	
				self.z = r1.array[2]	
				### pygame.draw.line(pygame.display.get_surface(),color, (self.cx+r1x*self.r,self.cy+r1y*self.r),(self.cx+r1x*self.r,self.cy+r1y*self.r))
				pygame.draw.line(pygame.display.get_surface(),color, (self.cx+self.x,self.cy+self.y),(self.cx+self.x,self.cy+self.y))
				self.i += .001 
			self.j += .001


    def draw4(self,color):
	self.i = 0 ## / medridians, / paralells
	self.j = 0###-2*3.14 ## / medridians, / paralells
	self.PI = 3.14152829
	self.cx = 100
	self.cy = 100
	self.cz = 100
	self.n = 100
	self.r = 100
	while self.j < self.n / 2:
		self.theta1 = self.j*2*self.PI / self.n - self.PI
		self.theta2 = (self.j+1)*2*self.PI / self.n - self.PI
		while self.i < self.n:
			self.theta3 = self.i * 2*self.PI / self.n
			self.x = cos(self.theta2)*cos(self.theta3) 
			self.y = sin(self.theta2)*sin(self.theta3)
			self.z = sin(self.theta2)
			self.x *= self.r
			self.y *= self.r
			self.z *= self.r
			pygame.draw.line(pygame.display.get_surface(),color, (self.cx+self.x,self.cy+self.y),(self.cx+self.x,self.cy+self.y))
			self.i += .001 
		self.j += .001

    def drawonmap(self, screen, maproom):
        self.imagescounter += 1
        if self.imagescounter == self.maximages:
            self.imagescounter = 0     
###        screen.scr.blit(self.images[self.imagescounter], (self.x+maproom.relativex, self.y+maproom.relativey))
	self.room = maproom
	relx = self.room.roomman.room.relativex
	rely = self.room.roomman.room.relativey
       	pygame.display.get_surface().blit(self.images[0], (self.x+relx,self.y+rely))
  
    def draw(self,room):
	self.room = room
	relx = self.room.roomman.room.relativex
	rely = self.room.roomman.room.relativey
       	pygame.display.get_surface().blit(self.images.getwithindex(0), (self.x+relx,self.y+rely))

