
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pygame
from pygame.locals import *


class Mouse(object):
    def __init__(self):
        self.position = None
        self.x = None
        self.y = None
        # for collision code
        self.w = 20
        self.h = 20
        self.update()
#        self.pressed = None
        self.previoustext = None

    def update(self):
        self.position = pygame.mouse.get_pos()
        self.x = self.position[0]
        self.y = self.position[1]

    def draw(self,screen,item):
        if item:
            screen.scr.blit(item.image, (self.x, self.y))

    # FIXME copy-paste entity
#     def collisionlist(self,collider,list):
#         for i in range(0,list.length):
#             o = None
#             o = list.getlistobject(o)
#             if o and collider.x >= o.x and collider.x <= o.x + o.w and collider.y >= o.y and collider.y <= o.y + o.h:   	
#                 return o
#         return None

#     def collisionlistrelative(self,collider,list,relx,rely):
#         for i in range(0,list.length):
#             o = None
#             o = list.getlistobject(o)

#             if o:
#                 #FIXME2
#                 x = o.x#self.processorrelativex(o,relx)
#                 y = o.y#self.processorrelativey(o,rely)

#                 mx = self.processorrelativex(self,relx)
#                 my = self.processorrelativey(self,rely)

#                 if mx >= x and mx <= x + o.w and my >= y and my <= y + o.h:
#                     return o
#         return None



    def collisionplayer(self,player):
        return player.collisionplayer(self)

    def collisionplayerrelative(self,player,relx,rely):
        return player.collisionplayerrelative(self,relx,rely)

    # generic method
    def press(self,buttonnumber):
        if  pygame.mouse.get_pressed()[buttonnumber]:
            return 1
            
        return None

    # left button press on player or entity
    def leftbuttonpressed(self,o,game):
        self.previoustext = o.leftbuttonpressed(game)#self,game.text,self.press(0))
        return self.previoustext
        
    def leftbuttonpressedwithinventoryitem(self,o,game):
        self.previoustext = o.leftbuttonpressedwithinventoryitem(game)
        return self.previoustext

#  

#     def middlebuttonpressed(self,o):
#         o.middlebutonpressed(self)

#     def rightbuttonpressed(self,o):
#         o.rightbutonpressed(self)
