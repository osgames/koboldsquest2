
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from enemytalking import *
from talksystemmyconid import *
from entityfactory import *

class Myconid(Enemytalking):
    def __init__(self,x,y,w,h):
        Enemytalking.__init__(self,x,y,w,h,Talksystemmyconid())
        self.addimage('./pics/myconid1.bmp',0,0,0)
        self.addimage('./pics/myconid2.bmp',0,0,0)
        self.addimage('./pics/myconid3.bmp',0,0,0)
        self.counter = 0


####### TEXT / FONT interface
    def getname(self,text):
        return text.getmyconidname()

#    def gettalk(self):
#        talksystem.gettalk(self)


####### other methods
    def draw(self,screen):
        self.counter += 1

        if self.counter >= 40:
            self.counter = 0

        if self.counter < 10:
            screen.scr.blit(self.images[0], (self.x, self.y))
        elif self.counter < 20:
            screen.scr.blit(self.images[1], (self.x, self.y))
        elif self.counter < 30:
            screen.scr.blit(self.images[2], (self.x, self.y))
        elif self.counter < 40:
            screen.scr.blit(self.images[1], (self.x, self.y))
        

#     def leftbuttonpressed(self,mouse,text):
#         return self.talksystem.gettalk(text)#foo

    def remove(self,list):
        if self.hitpoints <= 0:
            list.remove(self)

	    # LEAVE & ADD FOOD AT RANDOM
	    if randint(0,4) == 3:
		print ("The myconid drops some food.")
        	#factory = Entityfactory()
                #o = factory.make(Entity.ENTITY_FOOD,
		o = Food(self.x,self.y,20,20)#FIXME Maproom x, y!
	        list.add(o)
	
    def leftbuttonpressedwithinventoryitem(self,game):
        text = Enemytalking.leftbuttonpressedwithinventoryitem(self,game)
	if text:
            print "You use the item on the myconid."
            return text
        else:
            return None
