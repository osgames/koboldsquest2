
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from list import *
from engine import *
from pygame import *
from pygame.locals import *
import pygame

class OcttreeLeaf:
    "Octotree Leaf"
    def __init__(self, x,y,z,w,h,d,depth):
	self.x = x 
	self.y = y 
	self.z = z 
	self.w = w 
	self.h = h 
	self.d = d 
	self.depth = 0
	self.collide = 0 
	
    def isleaf(self):
	return 1

class OcttreeNode(OcttreeLeaf):
    "Octotree Node"
    def __init__(self,x,y,z,w,h,d,depth):
        OcttreeLeaf.__init__(self,x,y,z,w,h,d,depth)
	self.nodes = List() 
	self.collide = 0 

    def isleaf(self):
	return 0

    def add(self,n):
        self.nodes.addobject(n)


class Octtree:
    "Octtree"
    def __init__(self,x,y,z,w,h,d,depth,room):
	self.tree = OcttreeNode(x,y,z,w,h,d,depth)
	self.depth = depth
	self.room = room
	self.diagonaloffset = 120
        self.image = pygame.image.load("./pics/tile1-rot-420x420.bmp").convert()
        self.image.set_colorkey((0,0,0))
	###FIXself.font  = room.roomman.font 
	
    def add(self,n):
        self.tree.addobject(n)

    def generatetree(self,border):
	self.generatetreerec(self.depth,self.tree,self.tree.x,self.tree.y,self.tree.z,self.tree.w,self.tree.h,self.tree.d,border)

    def generatetreerec(self,depth,node,x,y,z,w,h,d,border):
	if depth <= 0:###FIXME1:
		for i in range(1,4):
			node.add(OcttreeLeaf(i*x/4,   y/4,    z/4,    w/3,h/3,d/3,depth))
		for i in range(1,4):
			node.add(OcttreeLeaf(i*x/4,   i*y/4,    z/4,    w/3,h/3,d/3,depth))
		for i in range(1,4):
			node.add(OcttreeLeaf(i*x/4,   i*y/4,    i*z/4,    w/3,h/3,d/3,depth))
		for i in range(1,4):
			node.add(OcttreeLeaf(x/4,   i*y/4,    z/4,    w/3,h/3,d/3,depth))
		for i in range(1,4):
			node.add(OcttreeLeaf(x/4,   i*y/4,    i*z/4,    w/3,h/3,d/3,depth))
		for i in range(1,4):
			node.add(OcttreeLeaf(i*x/4,   y/4,    i*z/4,    w/3,h/3,d/3,depth))
		for i in range(1,4):
			node.add(OcttreeLeaf(x/4,   y/4,    i*z/4,    w/3,h/3,d/3,depth))
		### center node
		for i in range(0,21):
			tempnode = node.nodes.getwithindex(i)	
		#	###FIXif border < tempnode.x*(self.depth-1) + tempnode.w:### and border > tempnode.x: ### + w/3:
		#	print "leaf border %d\n" % (tempnode.x*(depth-1)
			### NOTE: vary for each room, e.g. depth search algorithm
			###FIXif border < tempnode.x + tempnode.w:
			###	border += border
			###	node.nodes.getwithindex(i).collide = 1
			if border < tempnode.x + tempnode.w:
				border += border
				node.nodes.getwithindex(i).collide = 1
		###node = leaf 
		return 
	for i in range(1,4):
		node.add(OcttreeNode(i*x/4,   y/4,    z/4,    w/3,h/3,d/3,depth))
	for i in range(1,4):
		node.add(OcttreeNode(i*x/4,   i*y/4,    z/4,    w/3,h/3,d/3,depth))
	for i in range(1,4):
		node.add(OcttreeNode(i*x/4,   i*y/4,    i*z/4,    w/3,h/3,d/3,depth))
	for i in range(1,4):
		node.add(OcttreeNode(x/4,   i*y/4,    z/4,    w/3,h/3,d/3,depth))
	for i in range(1,4):
		node.add(OcttreeNode(x/4,   i*y/4,    i*z/4,    w/3,h/3,d/3,depth))
	for i in range(1,4):
		node.add(OcttreeNode(i*x/4,   y/4,    i*z/4,    w/3,h/3,d/3,depth))
	for i in range(1,4):
		node.add(OcttreeNode(x/4,   y/4,    i*z/4,    w/3,h/3,d/3,depth))
	### center node
	###OBSOLETEfor i in range(0,21):
	###	tempnode = node.nodes.getwithindex(i)	
	###	if border < tempnode.x:
	###		node.nodes.getwithindex(i).collide = 1

	for i in range(1,4):
		self.generatetreerec(depth-1,node.nodes.getwithindex(i),i*x/4,    y/4,    z/4,   w/3,h/3,d/3,border)
	for i in range(1,4):
		self.generatetreerec(depth-1,node.nodes.getwithindex(i+2),i*x/4,    i*y/4,    z/4,   w/3,h/3,d/3,border)
	for i in range(1,4):
		self.generatetreerec(depth-1,node.nodes.getwithindex(i+5),i*x/4,    i*y/4,    i*z/4,   w/3,h/3,d/3,border)
	for i in range(1,4):
		self.generatetreerec(depth-1,node.nodes.getwithindex(i+8),x/4,    i*y/4,    z/4,   w/3,h/3,d/3,border)
	for i in range(1,4):
		self.generatetreerec(depth-1,node.nodes.getwithindex(i+11),x/4,    i*y/4,    i*z/4,   w/3,h/3,d/3,border)
	for i in range(1,4):
		self.generatetreerec(depth-1,node.nodes.getwithindex(i+14),i*x/4,    y/4,    i*z/4,   w/3,h/3,d/3,border)
	for i in range(1,4):
		self.generatetreerec(depth-1,node.nodes.getwithindex(i+17),x/4,    y/4,    i*z/4,   w/3,h/3,d/3,border)
	### center node


    def collision(self,player,game):
	self.result = 0
	self.collisionrec(self.depth,self.tree,player,game)
	return self.result

    def collisionx(self,player):
	self.result = 0
	self.collisionrecx(self.depth,self.tree,player.x,player.y,player.z)
	return self.result

    def collisiony(self,player):
	self.result = 0
	self.collisionrecy(self.depth,self.tree,player.x,player.y,player.z)
	return self.result

    def collisionrec(self,depth,node,player,game):
	x = player.x
	y = player.y
	z = player.z
	###if depth <= 1 or node.isleaf():
	relx = self.room.roomman.room.relativex
	rely = self.room.roomman.room.relativey
	###if node.isleaf():
	###print "leaf! depth=%d" % depth
	if (x + relx < node.x +  node.w*(-depth+1) and x + relx + node.w > node.x) and (y + rely < node.y + node.h*(-depth+1) and y + rely > node.y):
		print "collision player.depth=%d map.depth=%d!" % (player.depth,depth)
		if depth <= player.depth: 
		###if node.collide == 1:### and node.w > 333:
			self.result = 1
			return
		elif player.depth < depth:
		###			node.depth -= 1	
			self.result = 2
			return
		elif player.depth > depth:	
			self.result = 3
			return
		###	###self.result = 1###
		###	player.jump(game,0)
		
		###return #NOTE do not return for each node to be switched
	###elif node.isleaf():
	###	return
        else:		
		for i in range(0,21):
			if not node.isleaf():
	 			n = node.nodes.getwithindex(i)
				if n != None: 
					self.collisionrec(depth-1,n,player,game)
		return
	
    def collisionrecx(self,depth,node,x,y,z):
	###if depth <= 1 or node.isleaf():
	relx = self.room.roomman.room.relativex
	rely = self.room.roomman.room.relativey
	if node.isleaf():
		##print "leaf!"
		return
	for i in range(0,21):
		n = node.nodes.getwithindex(i)
		if (n != None):
			if (x < n.x + n.w and x > n.x): 
				print "collision!"
				if n.collide == 1:
					self.result = 1
					return
			self.collisionrecx(depth-1,n,x,y,z)	

    def collisionrecy(self,depth,node,x,y,z):
	###if depth <= 1 or node.isleaf():
	relx = self.room.roomman.room.relativex
	rely = self.room.roomman.room.relativey
	if node.isleaf():
		##print "leaf!"
		return
	for i in range(0,21):
		n = node.nodes.getwithindex(i)
		if (n != None):
			if (y < n.y + n.h and y > n.y): 
				print "collision!"
				if n.collide == 1:
					self.result = 1
					return
			self.collisionrecx(depth-1,n,x,y,z)	

    def walkin(self,player):
	self.result = 0
	self.walkinrec(self.depth,self.tree,player)
	return self.result

    def walkinrec(self,depth,node,player):
	px = player.x
	py = player.y
	pz = player.z
	relx = self.room.roomman.room.relativex
	rely = self.room.roomman.room.relativey
	dd = self.depth-1 
	## SE dagonal orientation
	if rely == relx:### or rely == relx-270:
			####FIXME node.z
			###if node.collide == 1:
		print "++++++++++++++++++++++++++++++collision in walkin..."
		self.result = 1	
		return
	if node.isleaf():
		print "leaf!"
		return
	for i in range(0,21):
		if node.nodes.getwithindex(i) != None:
			relx = self.room.roomman.room.relativex
			rely = self.room.roomman.room.relativey
			###print "px=%d py=%d to relx=%d rely=%d\n" % (px, py,relx,rely)
			if px + self.room.roomman.room.relativex < node.nodes.getwithindex(i).x*(depth-1):
				self.walkinrec(depth-1,node.nodes.getwithindex(i),player)
   
    def draw(self,player,screen,rotx,roty,rotz):
	self.result = 0
	self.drawrec(self.depth,self.tree,player,rotx,roty,rotz)
##	a = pygame.surfarray.pixels3d(pygame.display.get_surface())
##	e = engine(rotx,roty,rotz)
##	relx = self.room.roomman.room.relativex
##	rely = self.room.roomman.room.relativey

##	for i in range(0,480):
##		for j in range(0,640):
##			c = a[j][i]
##			color = (c[0],c[1],c[2])
##			r1 = Vector3(j,i,self.depth*10)
##			if rotx:
##				r1 = e.mx.multiply(r1)
##			if roty:
##				r1 = e.my.multiply(r1)
##			if rotz:
##				r1 = e.mz.multiply(r1)
##			r1x = r1.array[0]	
##			r1y = r1.array[1]	
##			r1z = r1.array[2]	
##			pygame.draw.line(pygame.display.get_surface(),color,(r1x+relx,r1y+rely),(r1x+relx,r1y+rely))
        ###screen.scr.blit(self.image, (0,0))
	return self.result

    def drawrec(self,depth,node,player,rotx,roty,rotz):
       	###FIXpygame.display.get_surface().blit(self.font.render("Loading...", 8, (255,255,255)), (140,320))
	###a = pygame.surfarray.pixels3d(self.image)###pygame.display.get_surface())
	px = player.x
	py = player.y
	pz = player.z
	relx = self.room.roomman.room.relativex
	rely = self.room.roomman.room.relativey
	dd = self.depth-1 
	## SE dagonal orientation
	###if depth <= 4 and not node.isleaf() and node.nodes.length > 0:###node.isleaf():### or not node.isleaf():
	if node.isleaf() and node.collide == 1:
		r1x = node.x	
		r1y = node.y 
		r1z = node.z
		r1w = node.w	
		r1h = node.h
		r1d = node.d
		###pygame.draw.line(pygame.display.get_surface(), (0,255,0), (r1x+relx,r1y+rely), (r1x+relx+r1w,r1y+rely))
		###pygame.draw.line(pygame.display.get_surface(), (0,255,0), (r1x+relx+r1w,r1y+rely), (r1x+relx+r1w,r1y+rely+r1h))
		###pygame.draw.line(pygame.display.get_surface(), (0,255,0), (r1x+relx+r1w,r1y+rely+r1h), (r1x+relx,r1y+rely+r1h))
		###pygame.draw.line(pygame.display.get_surface(), (0,255,0), (r1x+relx,r1y+rely+r1h), (r1x+relx,r1y+rely))

		for i in range(0,21):###FIXME1
			e = engine(rotx,roty,rotz)

			r1 = Vector3(node.x,node.y,node.z)
			r2 = Vector3(node.x,node.y,node.z)
			if rotx:
				r1 = e.mx.multiply(r1)
				r2 = e.mx.multiply(r2)
			if roty:
				r1 = e.my.multiply(r1)
				r2 = e.my.multiply(r2)
			if rotz:
				r1 = e.mz.multiply(r1)
				r2 = e.mz.multiply(r2)
			r1x = r1.array[0]	
			r1y = r1.array[1]	
			r1z = r1.array[2]	
			r2x = r2.array[0]	
			r2y = r2.array[1]	
			r2z = r2.array[2]
			color = (255,0,0)
			if node.collide:
				color = (0,0,255)
			pygame.draw.line(pygame.display.get_surface(), color, (r1x+relx,r1y+rely), (r2x+relx,r2y+rely))
			###pygame.draw.line(pygame.display.get_surface(), color, (r1x+relx+n1.w*(self.depth-1),r1y+rely+n1.h*(self.depth-1)), (r2x+relx+n2.w*(self.depth-1),r2y+rely+n2.h*(self.depth-1)))
			###pygame.draw.line(pygame.display.get_surface(), color, (r1x+relx+n1.w*(self.depth-1),r1y+rely+n1.h*(self.depth-1)), (r2x+relx+n2.w*(self.depth-1),r2y+rely+n2.h*(self.depth-1)))
			###pygame.draw.line(pygame.display.get_surface(), color, (r1x+relx,r1y+rely), (r2x+relx+n2.w*(self.depth-1),r2y+rely+n2.h*(self.depth-1)))
			###pygame.draw.line(pygame.display.get_surface(), color, (r1x+relx+n1.w*(self.depth-1),r1y+rely+n1.h*(self.depth-1)), (r2x+relx,r2y+rely))
	###if depth <= 0: 
	###	return
       		pygame.display.get_surface().blit(self.image, (r1x+relx,r1y+rely))
	##for j in range(0,node.w):
	##		for i in range(0,node.h):
	##			print "--->%d" % (i)
	##			c = a[i%24][j%24]
	##			color = (c[0],c[1],c[2])	
	##			pygame.draw.line(pygame.display.get_surface(),color,(r1x+i+relx,r1y+j+rely),(r1x+i+relx,r1y+j+rely))

	for i in range(0,21):
		if not node.isleaf() and node.nodes.getwithindex(i) != None:
			self.drawrec(depth-1,node.nodes.getwithindex(i),player,rotx,roty,rotz)

    def draw2(self,player,screen):
	self.result = 0
	self.drawrec2(self.depth,self.tree,player)
	return self.result

    def drawrec2(self,depth,node,player):
	px = player.x
	py = player.y
	pz = player.z
	relx = self.room.roomman.room.relativex
	rely = self.room.roomman.room.relativey
	dd = self.depth-1 
	## SE dagonal orientation
	###if depth <= 4 and not node.isleaf() and node.nodes.length > 0:###node.isleaf():### or not node.isleaf():
	if node.isleaf():
		r1x = node.x	
		r1y = node.y 
		r1z = node.z
		r1w = node.w	
		r1h = node.h
		r1d = node.d
		pygame.draw.line(pygame.display.get_surface(), (255,0,0), (r1x+relx,r1y+rely), (r1x+relx+r1w,r1y+rely))
		pygame.draw.line(pygame.display.get_surface(), (255,0,0), (r1x+relx+r1w,r1y+rely), (r1x+relx+r1w,r1y+rely+r1h))
		pygame.draw.line(pygame.display.get_surface(), (255,0,0), (r1x+relx+r1w,r1y+rely+r1h), (r1x+relx,r1y+rely+r1h))
		pygame.draw.line(pygame.display.get_surface(), (255,0,0), (r1x+relx,r1y+rely+r1h), (r1x+relx,r1y+rely))

	for i in range(0,21):
		if not node.isleaf() and node.nodes and node.nodes.length > 0 and node.nodes.getwithindex(i) != None:
			self.drawrec2(depth-1,node.nodes.getwithindex(i),player)
    

    def draw3(self,player,screen):
	self.result = 0
	self.drawrec3(self.depth,self.tree,player)
	return self.result


    def drawrec3(self,depth,node,player,rotx,roty,rotz):
	px = player.x
	py = player.y
	pz = player.z
	relx = self.room.roomman.room.relativex
	rely = self.room.roomman.room.relativey
	dd = self.depth-1 
	## SE dagonal orientation
	###if depth <= 4 and not node.isleaf() and node.nodes.length > 0:###node.isleaf():### or not node.isleaf():
	if depth <= 4 and not node.isleaf() and node.nodes.length > 17:###node.isleaf():### or not node.isleaf():
		print "leaf!"
		for i in range(0,21):###FIXME1
			n2 = node.nodes.getwithindex(i)
			for j in range(0,21):
				n1 = node.nodes.getwithindex(j)
				e = engine(rotx,roty,rotz)

				r1 = Vector3(n1.x,n1.y,n1.z)
				r2 = Vector3(n2.x,n2.y,n2.z)
				if rotx:
					r1 = e.mx.multiply(r1)
					r2 = e.mx.multiply(r2)
				if roty:
					r1 = e.my.multiply(r1)
					r2 = e.my.multiply(r2)
				if rotz:
					r1 = e.mz.multiply(r1)
					r2 = e.mz.multiply(r2)
				r1x = r1.array[0]	
				r1y = r1.array[1]	
				r1z = r1.array[2]	
				r2x = r2.array[0]	
				r2y = r2.array[1]	
				r2z = r2.array[2]
				color = (255,0,0)
				if n2.collide or n1.collide:
					color = (0,0,255)
				##pygame.draw.line(pygame.display.get_surface(), color, (r1x+relx,r1y+rely), (r2x+relx,r2y+rely))
				##pygame.draw.line(pygame.display.get_surface(), color, (r1x+relx+n1.w*(self.depth-1),r1y+rely+n1.h*(self.depth-1)), (r2x+relx+n2.w*(self.depth-1),r2y+rely+n2.h*(self.depth-1)))
				##pygame.draw.line(pygame.display.get_surface(), color, (r1x+relx,r1y+rely), (r2x+relx+n2.w*(self.depth-1),r2y+rely+n2.h*(self.depth-1)))
				##pygame.draw.line(pygame.display.get_surface(), color, (r1x+relx+n1.w*(self.depth-1),r1y+rely+n1.h*(self.depth-1)), (r2x+relx,r2y+rely))
		###if depth <= 0: 
		###	return

	if node.isleaf():
		r1x = node.x	
		r1y = node.y 
		r1z = node.z
		r1w = node.w	
		r1h = node.h
		r1d = node.d
		pygame.draw.line(pygame.display.get_surface(), (0,255,0), (r1x+relx,r1y+rely), (r1x+relx+r1w,r1y+rely))
		pygame.draw.line(pygame.display.get_surface(), (0,255,0), (r1x+relx+r1w,r1y+rely), (r1x+relx+r1w,r1y+rely+r1h))
		pygame.draw.line(pygame.display.get_surface(), (0,255,0), (r1x+relx+r1w,r1y+rely+r1h), (r1x+relx,r1y+rely+r1h))
		pygame.draw.line(pygame.display.get_surface(), (0,255,0), (r1x+relx,r1y+rely+r1h), (r1x+relx,r1y+rely))
		a = pygame.surfarray.pixels3d(self.image)###pygame.display.get_surface())

		for j in range(0,24):
			for i in range(0,24):
				print "--->%d" % (i)
				c = a[i][j]
				color = (c[0],c[1],c[2])	
				pygame.draw.line(pygame.display.get_surface(),color,(r1x+i+relx,r1y+j+rely),(r1x+i+relx,r1y+j+rely))

	for i in range(0,21):
		### if not node.isleaf() and node.nodes and node.nodes.length > 0 and node.nodes.getwithindex(i) != None:
		if not node.isleaf() and node.nodes.getwithindex(i) != None:
			###relx = self.room.roomman.room.relativex
			###rely = self.room.roomman.room.relativey
			###print "px=%d py=%d to relx=%d rely=%d\n" % (px, py,relx,rely)
			##if px + self.room.roomman.room.relativex < node.nodes.getwithindex(i).x*(depth-1):
			self.drawrec3(depth-1,node.nodes.getwithindex(i),player,rotx,roty,rotz)
	
