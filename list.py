
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


class List:
    def __init__(self):
        self.list = []
        self.length = 0
        self.index = 0

    def add(self,o):
        o.addtolist(self)

    def addobject(self,o):
        self.length += 1
        self.list.append(o)
    
    def remove(self,o):
        resultlist = []
        self.length = 0
        
        for el in self.list:
            if  el != o:
                resultlist.append(el)
                self.length += 1
        self.list = resultlist
                

    def getlistobject(self,o):#NOTE1 o not list
        if self.index < self.length:
            o = self.list[self.index]
            self.index += 1
            return o
        else:
            self.index = 0
        return None

    def getwithindex(self,index):
        if index < self.length:
            return self.list[index]

        return None

#    def resetindex(self,idx):
#        self.index = idx
        
