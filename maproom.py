
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pygame
from pygame.locals import *
from room import *
from octtree import *

class Maproom(Room):
    "Room with a (big) map"
    def __init__(self,x,y,font):
        Room.__init__(self)
        self.relativex = x
        self.relativey = y
	self.offset = 0#200 KLUDGE
	### self.roomman = roomman
	###FIXself.octtree = Octtree(1000,1000,1000,1000,1000,1000,3,self,font)
	###self.octtree = Octtree(1000,1000,1000,1000,1000,1000,0,self)
	self.trees = List()

    def draw(self,screen):
            ##if self.level.roommanager.room.octtree.walkin(self.player):
	    ###	print "Collision!"
	###	self.level.roommanager.room.roomman.room.undomove(self.player)
        rotx = 2/sqrt(2)###-3.141528/2 
        roty = 0#2/sqrt(2)###-3.141528/2 
        rotz = 0#2/sqrt(2)###-3.141528/2 
	    ##roty = 0 
	    ##rotz = 0 
	screen.scr.blit(self.background, (self.offset+self.relativex, 0+self.relativey))
	for o in self.trees.list:
        	if o.draw(self.player,screen,rotx,roty,rotz):
			1

        screen.drawonmap(self)

    def update(self,player,level,swordplay):
        self.updatelist(self.entities)

        o = player.collisionlistrelative(self.entities,self.relativex,self.relativey)
###        if o:
###	    if swordplay != 0:
###		print("+++++++++++++++SWORDPLAY")
###		o.hit()#points -= 1
  ###      	if o.hitpoints <= 0:
    ###        	    self.entities.remove(o)    
###	    else:
###	        o.dofood(player)
###	    	o.doplatform(player,level)
###               	player.bounceback(level)
##                o.decreasehpinplayer(player,level)
#            	o.remove(self.entities)

	## FIXME memento pattern
    def undomove(self,player):
	if player.direction == "north":   
        	self.relativey = self.relativey - 10
        	self.relativey = self.relativey - 10
        	self.relativey = self.relativey - 10
	if player.direction == "south":     
        	self.relativey = self.relativey + 10
        	self.relativey = self.relativey + 10
        	self.relativey = self.relativey + 10
	if player.direction == "west":     
        	self.relativex = self.relativex - 10
        	self.relativex = self.relativex - 10
        	self.relativex = self.relativex - 10
	if player.direction == "east":     
        	self.relativex = self.relativex + 10
        	self.relativex = self.relativex + 10
        	self.relativex = self.relativex + 10
 
    def moveup(self,player):
        player.direction = "north"
###	if self.relativey > 0:###FIXME
       	self.relativey = self.relativey + 10

    def movedown(self,player):
        player.direction = "south"
        self.relativey = self.relativey - 10

    def moveleft(self,player):
        player.direction = "west"
        self.relativex = self.relativex + 10

    def moveright(self,player):
        player.direction = "east"
        self.relativex = self.relativex - 10


    #### COLLISION CODE - OVERRIDING ROOM CODE 


    def processorrelativex(self,o):
        return o.x - self.relativex

    def processorrelativey(self,o):
        return o.y - self.relativey

    def collisionlistbombrelative(self,bomb,list):
        for o in list.list:
            if o:
                x = self.processorrelativex(o)
                y = self.processorrelativey(o)
                if x + 12>= bomb.x - bomb.w and x <= bomb.x + bomb.w +12 and y+12 >= bomb.y - bomb.h and y <= bomb.y + bomb.h +12:
                    return o
        return None

#     def warp(self,x,y):
#         self.relativex = 800
#         self.relativey = 800


    def collisionexits(self,player,roommanager):
        o = player.collisionlistrelative(self.exits, self.relativex, self.relativey)

        if o:
            print 'exit foo x=%d,y=%d px=%d py=%d' % (o.x,o.y,player.x,player.y)
            o.room.loadroom()
            roommanager.room = o.room
            player.warp(o.newx,o.newy)
            return 1
        return None

    def collisionboxes(self,player):
        o = player.collisionlistrelative(self.boxes, self.relativex, self.relativey)
        if o:
            #player.bounceback()
            return 1
        return None


    def collisionandremoveenemieswithbomb(self, bomb, player, damagevalue):

        b = Bomb(self.processorrelativex(bomb),self.processorrelativey(bomb),80,80,player)
#        bomb.x = self.processorrelativex(bomb.x)
#        bomb.y = self.processorrelativey(bomb.y)
        o = self.collisionlistbombrelative(b,self.entities)
        if o:
            retvalue = o.hit()
	    o.hitcommand(player, "bomb",retvalue, damagevalue)
            o.remove(self.entities)
            ### self.entities.remove(o)
            
    def summonskeletonunderplayercommand(self,x,y):
        self.addenemy(Skeletonunderplayercommand(x,y,50,50))


    def summonskeleton(self,x,y):
        self.addenemy(Skeleton2(x-self.relativex,y-self.relativey,50,50))


    # Dropping bomb entities
    def addsprite(self,sprite):
        sprite.x -= self.relativex
        sprite.y -= self.relativey
        self.addentity(sprite)


    def collisionplayerrelative(self,mouse,player,relx,rely):
        return mouse.collisionplayerrelative(player,relx,rely)

    def domouse(self,game):
        # Collide mouse with player
        p = game.domouseonplayerrelative(self)#game.collisionplayerrelative(self)
        if p:
            print ("mouse click on player 1")
            foo = game.leftbuttonpressed(game.player)
            if foo:
		print ("mouse click on player 12")
                return
            
        # Collide mouse with entities
        o = None
        o = game.collisionlistrelativeonmouse(self.entities,self.relativex,self.relativey)
        if o:
            foo = game.mouse.leftbuttonpressed(o,game)
#            print foo
            if foo:
		### BLIT RETURN TALK TEXT
                game.screen.scr.blit(game.font.render(foo, 16, (255,255,255)), (20,330))
#                print foo
            else:
                game.screen.scr.blit(game.font.render(o.getname(game.text), 10, (0,0,0)), (game.mouse.x+10,game.mouse.y))
#            mouse.middlebuttonpressed(o,text)
#            mouse.rightbuttonpressed(o,text)
            
                
    def domouseinventory(self,game):#,screen,mouse,font,text,item,player):
        # Collide mouse with player
        p = game.domouseonplayerrelative(self)
        if p:
            footext4 = game.mouse.leftbuttonpressedwithinventoryitem(game.player,game)
            if footext4 and footext4 != "":
                return footext4 

        # Collide mouse with entities
        o = None
        o = game.collisionlistrelativeonmouse(self.entities,self.relativex,self.relativey)
        if o and game.inventoryitem:
            footext5 = game.mouse.leftbuttonpressedwithinventoryitem(o,game)#.text,game.inventoryitem)
            if footext5 and footext5 != "":
                return footext5 

        if not o and game.mouse.press(0):
            return 1

        return None

