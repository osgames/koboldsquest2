
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from entity import *

class Enemy(Entity):
    def __init__(self,x,y,w,h):
        Entity.__init__(self,x,y,w,h)

        self.decreasehp = 1
        self.hitpoints = 1

        self.direction = "north"

    def hit(self):
        r = randint(0,20)
        if r >= 1:
            print "hit"
            self.hitpoints -= self.decreasehp
        else:
            print "miss"
        
    def remove(self,list):
        if self.hitpoints <= 0:
            list.remove(self)

    def bounceback(self):
        if self.direction == "north":
            self.y += 2 
            #FIXME self.direction = "south"
        elif self.direction == "south":
            self.y -= 2
            #self.moveup()
        elif self.direction == "west":
            self.x += 2
            #self.moveright()
        elif self.direction == "east":
            self.x -= 2
            #self.moveleft()


    def decreasehpinplayer(self, player, level):
	player.decreasehp(level)	
            
