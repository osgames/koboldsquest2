
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pygame
from pygame.locals import *

class Player:
    "Player"
    def __init__(self):
	#Entity.__init__(self,200,210,50,50)
        self.x = 40#400
        self.y = 210#100
        self.z = 110#100
        self.w = 50
        self.h = 50
        self.d = 50
        self.depth = 0
        self.direction = "south"
        self.images = []
        self.images2 = []
        self.images3 = []
        self.images4 = []

        self.image1 = pygame.image.load('./pics/rogueassassinleft1.bmp').convert()
        self.image2 = pygame.image.load('./pics/rogueassassinleft2.bmp').convert()
        self.image3 = pygame.image.load('./pics/rogueassassinleft3.bmp').convert()
        self.image4 = pygame.image.load('./pics/rogueassassinleft2.bmp').convert()
        self.image1.set_colorkey((0,0,0)) 
        self.image2.set_colorkey((0,0,0)) 
        self.image3.set_colorkey((0,0,0)) 
        self.image4.set_colorkey((0,0,0)) 

        self.image5 = pygame.image.load('./pics/rogueassassinright1.bmp').convert()
        self.image6 = pygame.image.load('./pics/rogueassassinright2.bmp').convert()
        self.image7 = pygame.image.load('./pics/rogueassassinright3.bmp').convert()
        self.image8 = pygame.image.load('./pics/rogueassassinright2.bmp').convert()
        self.image5.set_colorkey((0,0,0)) 
        self.image6.set_colorkey((0,0,0)) 
        self.image7.set_colorkey((0,0,0)) 
        self.image8.set_colorkey((0,0,0)) 

        self.image9 = pygame.image.load('./pics/rogueassassinup1.bmp').convert()
        self.image10 = pygame.image.load('./pics/rogueassassinup2.bmp').convert()
        self.image11 = pygame.image.load('./pics/rogueassassinup3.bmp').convert()
        self.image12 = pygame.image.load('./pics/rogueassassinup2.bmp').convert()
        self.image9.set_colorkey((0,0,0)) 
        self.image10.set_colorkey((0,0,0)) 
        self.image11.set_colorkey((0,0,0)) 
        self.image12.set_colorkey((0,0,0)) 
 
        self.image13 = pygame.image.load('./pics/rogueassassindown1.bmp').convert()
        self.image14 = pygame.image.load('./pics/rogueassassindown2.bmp').convert()
        self.image15 = pygame.image.load('./pics/rogueassassindown3.bmp').convert()
        self.image16 = pygame.image.load('./pics/rogueassassindown2.bmp').convert()
        self.image13.set_colorkey((0,0,0)) 
        self.image14.set_colorkey((0,0,0)) 
        self.image15.set_colorkey((0,0,0)) 
        self.image16.set_colorkey((0,0,0)) 

        self.imageindex = 0
        self.addimages()

        self.talkcounter = 0
        self.currenttext = ""
    
    def addimages(self):
        self.images.append(self.image1)
        self.images.append(self.image2)
        self.images.append(self.image3)
        self.images.append(self.image4)
        self.images2.append(self.image5)
        self.images2.append(self.image6)
        self.images2.append(self.image7)
        self.images2.append(self.image8)
        self.images3.append(self.image9)
        self.images3.append(self.image10)
        self.images3.append(self.image11)
        self.images3.append(self.image12)
        self.images4.append(self.image13)
        self.images4.append(self.image14)
        self.images4.append(self.image15)
        self.images4.append(self.image16)

    def warp(self,x,y):
        self.x = x
        self.y = y


    def moveupanddirect(self,level):
        level.roommanager.room.moveup(self)

    def movedownanddirect(self,level):
        level.roommanager.room.movedown(self)

    def moveleftanddirect(self,level):
        level.roommanager.room.moveleft(self)

    def moverightanddirect(self,level):
        level.roommanager.room.moveright(self)


    def moveupanddirect1(self):
        self.direction = "north"
        self.y = self.y - 12
        self.imageindex = self.imageindex + 1

    def movedownanddirect1(self):
        self.direction = "south"
        self.y = self.y + 12
        self.imageindex = self.imageindex + 1

    def moveleftanddirect1(self):
        self.direction = "west"
        self.x = self.x - 12
        self.imageindex = self.imageindex + 1

    def moverightanddirect1(self):
        self.direction = "east"
        self.x = self.x + 12
        self.imageindex = self.imageindex + 1

    def moveup(self):
        self.y = self.y - 12

    def movedown(self):
        self.y = self.y + 12

    def moveleft(self):
        self.x = self.x - 12

    def moveright(self):
        self.x = self.x + 12


    def bounceback(self,level):
        if self.direction == "north":
            self.movedownanddirect(level)
            #self.movedown()
        elif self.direction == "south":
            self.moveupanddirect(level)
            #self.moveup()
        elif self.direction == "west":
            self.moverightanddirect(level)
            #self.moveright()
        elif self.direction == "east":
            self.moveleftanddirect(level)
            #self.moveleft()


    def draw(self):
        self.imageindex = self.imageindex + 1
        if self.imageindex > 3:
            self.imageindex = 0

	if self.direction == "west":
            return self.images[self.imageindex]
        elif self.direction == "east": 
            return self.images2[self.imageindex]
	elif self.direction == "north":
            return self.images3[self.imageindex]
        elif self.direction == "south": 
            return self.images4[self.imageindex]

    def draw0(self):
        self.imageindex = 0
	if self.direction == "west":
            return self.images[0]
        elif self.direction == "east": 
            return self.images2[0]
	elif self.direction == "north":
            return self.images3[0]
        elif self.direction == "south": 
            return self.images4[0]

    def warp(self,x,y):
        self.x = x
        self.y = y


    def moveupanddirect(self,level):
        level.roommanager.room.moveup(self)

    def movedownanddirect(self,level):
        level.roommanager.room.movedown(self)

    def moveleftanddirect(self,level):
        level.roommanager.room.moveleft(self)

    def moverightanddirect(self,level):
        level.roommanager.room.moveright(self)


    def moveupanddirect1(self):
        self.direction = "north"
        self.y = self.y - 12
        self.imageindex = self.imageindex + 1

    def movedownanddirect1(self):
        self.direction = "south"
        self.y = self.y + 12
        self.imageindex = self.imageindex + 1

    def moveleftanddirect1(self):
        self.direction = "west"
        self.x = self.x - 12
        self.imageindex = self.imageindex + 1

    def moverightanddirect1(self):
        self.direction = "east"
        self.x = self.x + 12
        self.imageindex = self.imageindex + 1

    def moveup(self):
        self.y = self.y - 12

    def movedown(self):
        self.y = self.y + 12

    def moveleft(self):
        self.x = self.x - 12

    def moveright(self):
        self.x = self.x + 12


    def decreasehp(self,level):
	print ("player is hit!")
        self.hitpoints -= self.decreasehitpoints

        if self.hitpoints <= 0:
            level.setgameover()
    
    def increasehp(self):
	if self.hitpoints < 30:
            self.hitpoints += 5 

    def collisionplayer(self,mouse):
        if mouse.x >= self.x and mouse.x <= self.x + self.w and mouse.y >= self.y and mouse.y <= self.y + self.h:   	
              return self
        return None

    def processorrelativex(self,o,relx):
        return o.x - relx

    def processorrelativey(self,o,rely):
        return o.y - rely

    def collisionplayerrelative(self,mouse,relx,rely):
        px = self.processorrelativex(self,relx)
        py = self.processorrelativey(self,rely)

        x = self.processorrelativex(mouse, relx)
        y = self.processorrelativey(mouse, rely)
        if x >= px and x <= px + self.w and y >= py and y <= py + self.h:   	
              return self
        return None

    def collisionlist(self,list):
        return self.collisionlist1(self,list)

    def collisionlistblocks(self,list):
        return self.collisionlistblocks1(self,list)

    # GEN PROC
    def collisionlist1(self,collider,list):
        for i in range(0,list.length):
            o = None
            o = list.getlistobject(o)
            
            if o:
#                print 'i=%d x=%d y=%d px=%d py=%d' % (i, o.x,o.y,collider.x,collider.y)
                if collider.x >= o.x and collider.x <= o.x + o.w and collider.y >= o.y and collider.y <= o.y + o.h:   	
                    return o
        return None

    # GEN PROC
    def collisionlistblocks1(self,collider,list):
        for i in range(0,list.length):
            o = None
            o = list.getlistobject(o)
            
            if o:
#                print 'i=%d x=%d y=%d px=%d py=%d' % (i, o.x,o.y,collider.x,collider.y)
                if collider.x >= o.x and collider.x <= o.x + o.w and collider.y >= o.y and collider.y <= o.y + o.h and collider.z <= o.z + o.d and collider.z >= o.z:   	
                    return o
        return None

	# SWORD COLLISON
    def collisionlist2(self,collider,list):
        for i in range(0,list.length):
            o = None
            o = list.getlistobject(o)
            
            if o:
#                print 'i=%d x=%d y=%d px=%d py=%d' % (i, o.x,o.y,collider.x,collider.y)
                if collider.x+30 >= o.x and collider.x <= o.x + o.w+30 and collider.y +30>= o.y and collider.y <= o.y + o.h +30:   	
                    return o
        return None

    #FIXME put in Utitlities
    def processorrelativex(self,o,relx):
        return o.x - relx

    def processorrelativey(self,o,rely):
        return o.y - rely
    
    def collisionlistrelative(self,list,relx,rely):
        return self.collisionlistrelative1(self,list,relx,rely)

    # GEN PROC
    def collisionlistrelative1(self,collider,list,relx,rely):
        for i in range(0,list.length):
            o = None
            o = list.getlistobject(o)

            if o:
       
                #FIXME1 o.x <-> self.relativex
                x = o.x - 12#self.processorrelativex(o,relx)
                y = o.y - 12#self.processorrelativey(o,rely)
                px = self.processorrelativex(self,relx)
                py = self.processorrelativey(self,rely)

#                print 'i=%d x=%d y=%d px=%d py=%d' % (i, o.x,o.y,px,py)
		# NOTE : +24 for better sword collision
                if px + 12 >= x and px <= x + o.w + 12 and py + 12 >= y and py <= y + o.h + 12:   	
                    return o
        return None

    def leftbuttonpressed(self,game):
        if game.mouse.press(0) == 1:
            self.talkcounter += 1
            #print "keypress on player"
            if self.talkcounter == 1:
                1#self.currenttext = self.talksystem.gettalk(text)#foo
            return self.currenttext
        else:
            self.talkcounter = 0 	
            return self.currenttext

    def jump(self,game,ascend):
        if game.level.roommanager.room.collisionboxes(self) or game.level.roommanager.room.octtree.collision(self,game):
		if ascend:
			self.depth -= 1
		else:
			self.depth += 1
	if self.direction == "north":
###		if ascend:
		self.moveupanddirect(game.level)
		self.moveupanddirect(game.level)
		self.moveupanddirect(game.level)
		self.moveupanddirect(game.level)
		self.moveupanddirect(game.level)
		self.moveupanddirect(game.level)
###		else:
###			self.movedownanddirect(game.level)
###			self.movedownanddirect(game.level)
###			self.movedownanddirect(game.level)
###			self.movedownanddirect(game.level)
###			self.movedownanddirect(game.level)
###			self.movedownanddirect(game.level)
	if self.direction == "south":
		self.movedownanddirect(game.level)
		self.movedownanddirect(game.level)
		self.movedownanddirect(game.level)
		self.movedownanddirect(game.level)
		self.movedownanddirect(game.level)
		self.movedownanddirect(game.level)
		self.movedownanddirect(game.level)
		self.movedownanddirect(game.level)
		self.movedownanddirect(game.level)
		self.movedownanddirect(game.level)
		self.movedownanddirect(game.level)
		self.movedownanddirect(game.level)
	if self.direction == "west":
		self.moveleftanddirect(game.level)
		self.moveleftanddirect(game.level)
		self.moveleftanddirect(game.level)
		self.moveleftanddirect(game.level)
		self.moveleftanddirect(game.level)
		self.moveleftanddirect(game.level)
	if self.direction == "east":
		self.moverightanddirect(game.level)
		self.moverightanddirect(game.level)
		self.moverightanddirect(game.level)
		self.moverightanddirect(game.level)
		self.moverightanddirect(game.level)
		self.moverightanddirect(game.level)
	###else:
	###	self.depth = 0 
