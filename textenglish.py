
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from text import *

class Textenglish(Text):


    def getname(self,o):
        return o.getnametext(self)

    def getnullname(self):
        return "..."
    
    def getbombmanname(self):
        return "Bomb Man"

    def getbombman1(self):
        return "Boom! Boom!"

    def getbombman2(self):
        return "Blast! Blast!"

    def getsword1bluename(self):
        return "Seraphim sword"

    def getcandlename(self):
        return "Candle"
    
    def getscrollinvisibilityname(self):
        return "Scroll of invisibility"
    
    def gethermitname(self):
        return "Hermit"
    
    def gettrollblackname(self):
        return "Black troll"

    def getoozebigname(self):
        return "Big blue ooze"

    def getoozename(self):
        return "Blue ooze"

    def gethoundarchonname(self):
        return "Hound archon"

    def getknightdarkname(self):
        return "Dark knight"

    def getmoneyname(self):
        return "Money"

    def getcapricornguardianname(self):
        return "Capricorn Guardian"

    def getcapricornguardian31(self):
        return "Do not fight Celestia!"

    def getcapricornguardian32(self):
        return "If you hurt me I will strike you!"

    def getsword1bluename(self):
        return "Seraphim sword"

    def getcandlename(self):
        return "Candle"
    
    def getscrollinvisibilityname(self):
        return "Scroll of invisibility"
    
    def gethermitname(self):
        return "Hermit"
    
    def gettrollblackname(self):
        return "Black troll"

    def getoozebigname(self):
        return "Big blue ooze"

    def getoozename(self):
        return "Blue ooze"

    def gethoundarchonname(self):
        return "Hound archon"

    def getknightdarkname(self):
        return "Dark knight"

    def getmoneyname(self):
        return "Money"

    def getfoodname(self):
        return "Food"

    def getchestname(self):
        return "Chest"

    def getunicornstatuename(self):
        return "Unicorn statue"

    def getringbluename(self):
        return "Blue ring"
    
    def getcherubstatuename(self):
        return "Cherub"
    
    def getbatname(self):
        return "Bat"
    
    def getcandleflame1name(self):
        return "Flame angel"

    def getmyconidbluename(self):
        return "Blue myconid"
    
    def getchaosknight1name(self):
        return "Dark Warrior"

    def getshadowbladename(self):
        return "Shadowblade"

    def getbirdsparrowname(self):
        return "Blue sparrow"

    def getnuttername(self):
        return "Nutter"

    def getnutter2name(self):
        return "Nutter"

    def getmyconidname(self):
        return "Myconid"
    
    def getshieldmaidenname(self):
        return "Elf"

    def getentname(self):
        return "Ent"

    def getskeletonname(self):
        return "Skeleton"

    def gettreename(self):
        return "Tree"

    def getblueorcname(self):
        return "Blue Orc"
    
    def gethobgoblinswordsmanname(self):
        return "Hobgoblin Swordsman"
    
    def getvillageorcmagename(self):
        return "Blue Orc Mage"

    def getorcpaladinname(self):
        return "Orc Paladin"
    
    def getorcswordsmanbluename(self):
        return "Orc swordsman"

    def getwaterdevilname(self):
        return "Water devil"

    def getwaterdevilfemalename(self):
        return "Water devil female"
    
    def getensorcelledtreename(self):
        return "Enscorcelled tree"

    def getangelname(self):
        return "Angel"
    
    def getdemon1name(self):
        return "Demon fighter"
    
    def getorchutname(self):
        return "Orc hut"
    
    def getearthweirdname(self):
        return "Earth weird"

    def getkeyname(self):
        return "Key"

    def getkey2name(self):
        return "Blue key"

    def getkey3name(self):
        return "Golden key"

    def getdracolichname(self):
        return "Dracolich"

    def getspellbookdracolichname(self):
        return "Dracolich's spellbook"

    def getskeletonofplayername(self):
        return "Skeleton under your command"

    def getkoboldmagename(self):
        return "Kobold mage"


    ############# NUTTER 

    def getnutter1(self):
        return "Hello. I am Stawie."

    def getnutter2(self):
        return "I am a disciple in magic. Nootie is my master."

    def getnutter3(self):
        return "He lives further north from here."

    def getnutter4(self):
        return "He is hard-hearing..so be patient with him."

    ############# NUTTER2 

    def getnutter21(self):
        return "We are nut people."

    def getnutter22(self):
        return "Green magic flows through us."


    ############# SPARROW 

    def getbirdsparrow1(self):
        return "There is a hole under the black tree."

    def getbirdsparrow2(self):
        return "All you have to do is remove the tree...haha!"

    #############  TROLL BLACKM

    def gettrollblack1(self):
        return "..gnuvy.."

    def gettrollblack2(self):
        return "*groink*"

    ############# HERMIT 

    def gethermit1(self):
        return "Use the Seraphim ring on walls."

    def gethermit2(self):
        return "You will be able to walk through."

    ############# CANDLEFLAME 2

    def getcandleflame21(self):
        return "This dungeon was a castle built on water."

    def getcandleflame22(self):
        return "We are the eternal flame which burns for this dungeon."

    def getcandleflame23(self):
        return "All elven lords of the castle went into these flames."

    def getcandleflame24(self):
        return "They are now one with the fire."

    def getcandleflame25(self):
        return "The fire contrasts the water, thus is our blessing."

    ############# CANDLEFLAME 1

    def getcandleflame11(self):
        return "We are fiery angels."

    def getcandleflame12(self):
        return "We are the Red Seraphim."

    def getcandleflame13(self):
        return "Wander into the flames if you must."

    def getcandleflame14(self):
        return "You will not be harmed."

    def getcandleflame15(self):
        return "That is our blessing."

    ############# DARK WARRIOR 

    def getchaosknight11(self):
        return "You will face defeat."

    def getchaosknight12(self):
        return "I will show no mercy."

    ############# MYCONID

    def getmyconid1(self):
        return "The earth is bright here."

    def getmyconid2(self):
        return "I'll learn you some magic."

    ############# EARTH WEIRD

    def getearthweird1(self):
        return "Hello."

    def getearthweird2(self):
        return "I'm an earth master."

    ############# KOBOLD MAGE

    def getkoboldmage1(self):
        return "Praise to Kurtulmak!"

    def getkoboldmage2(self):
        return "Bring me the secret of the dark."


    ############# ORCBLUE1 

    def getorcblue11(self):
        return "This is Staw .. Orc Village."

    def getorcblue12(self):
        return "We do not have many guests here."

    ############# VILLAGE ORC 2 

    def getvillageorc21(self):
        return "This is Staw .. Orc Village."

    def getvillageorc22(self):
        return "We do not have many guests here."

    ############# ORC VILLAGE 3 (not MAGE) 

    def getvillageorc31(self):
        return "I do not want to talk to you."

    def getvillageorc32(self):
        return "Go Away."

    ############# ORC VILLAGE 2 (MAGE) 

    def getvillageorc11(self):
        return "Kobold magi .. torture .. twilight .."

    def getvillageorc12(self):
        return "Do not travel to the north .. "

    ############# WATER DEVIL FEMALE 

    def getwaterdevilfemale1(self):
        return "Take the Elven sword."
    def getwaterdevilfemale2(self):
        return "It has a red Seraphim in it which guides you through the battle"
    def getwaterdevilfemale3(self):
        return "This water fortress was built by high elves"
    def getwaterdevilfemale4(self):
        return "Evil cursed the landlords and they are now hither"
    def getwaterdevilfemale5(self):
        return "Witch elves destroyed the keep"
    def getwaterdevilfemale6(self):
        return "I am the last elven maiden left"
    def getwaterdevilfemale7(self):
        return "I was condemned into this form"
    def getwaterdevilfemale8(self):
        return "My hope is with you."
    def getwaterdevilfemale9(self):
        return "Please defeat the evil which overtook us."
        return "You are our last hope."
    ############# WATER DEVIL FEMALE 

    def getwaterdevil1(self):
        return ".blurbs..."

    def getwaterdevil2(self):
        return "...splat.."

    ############# ORC SWORDSMAN

    def getorcswordsmanblue1(self):
        return "We were elves once."

    def getorcswordsmanblue2(self):
        return "But darkness took us..."

    ############# ORC WARRIOR VILLAGE

    def getorcpaladin1(self):
        return "I defend the good cause."

    def getorcpaladin2(self):
        return "May the gods be with you..."

    ############ SHIELD MAIDEN
    def getshieldmaiden1(self):
        return "These mountains connect the Elf Tower with the west."
    def getshieldmaiden2(self):
        return "The elves have gone now."
    def getshieldmaiden3(self):
        return "They left the tower ages ago."

    ############ HOBGOBLIN SWORDSMAN 1   
    def gethobgoblinswordsman1(self):
        return "..."
    def gethobgoblinswordsman2(self):
        return "..."
    
    ############# HOUND ARCHON 

    def gethoundarchon1(self):
        return "Red angels do not thrive in water."

    def gethoundarchon2(self):
        return "Do not venture too deep in the water.."

    ############# ANGEL 

    def getangel1(self):
        return "Try to find a good person and share with him."

    def getangel2(self):
        return "Celestia is watching you."

    ############# DEMON1 

    def getdemon11(self):
        return "Hissssss!"

    def getdemon12(self):
        return "Darkness will prevail ...!"

    def getangelusedspellbook(self):
        return "The book disappears in the angel's hands!"

    ############# CHERUBSTATUE1 

    def getcherubstatue11(self):
        return "May god bless you."

    def getcherubstatue12(self):
        return "Take the ring of the Seraphim next to me."

    def getcherubstatue13(self):
        return "May it aid you upon your quest."

    def getcherubstatue14(self):
        return "Watch out for the dark side of the lands."


