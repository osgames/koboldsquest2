
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pygame
from pygame.locals import *

from entity import *

from random import *
import os

from utility import *

class Scrollinvisibility(Entity):
    def __init__(self,x,y,w,h,dungeonnumber,keynumber):
        Entity.__init__(self,x,y,w,h)
        self.addimage('./pics/scroll1.bmp',255,255,255)

        self.counter = 0
        self.dungeonnumber = dungeonnumber
        self.keynumber = keynumber

    def update(self,level):
        1

    def deathupdate(self):
        1
        
    #FIXME copy-paste in dracolich.py
    def remove(self,list):
        string = "You have a scroll of invisibility "
        string += self.keynumber
        string += " - dungeon "
        string += self.dungeonnumber
        string += "\n"

        s = ""

        if Utilities().readandfindstring(string):
            list.remove(self)
            return None
        
        f = open("./workfile",'r')
        for line in f:
            s += line

        f = open("./workfile",'w')
        
        f.write(s)
        f.write(string)
        f.write("\n")

        return 1
 
    #FIXME copy-paste in dracolich.py readboss
    def readkeys(self, list):
        string = "You have a scroll of invisibility "
        string += self.keynumber
        string += " - dungeon "
        string += self.dungeonnumber
        string += "\n"

        if Utilities().readandfindstring(string):
            print "+++ Scroll of invisibility registered"
            if list:
                list.remove(self)
            return 1

        return None

    def getname(self,text):
        return text.getscrollinvisibilityname()
